var express = require('express'); //Importamos paquete express
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var app = express(); //Inicializamos
app.use(bodyParser.json());

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancobmr/collections";
var apiKey = "apiKey=vUKW5c6Yts567ZV0ye44_dPgQWlBExFK";
var clienteMlab =null;

var port = process.env.PORT || 3000; //Queremos escuchar en el puerto 3000
//var usuarios = require('./usuarios.json');
var usuarios = require('./logins.json');
var logins = require('./logins.json');
var cuentas = require('./cuentas.json');
var fs = require('fs');

//console.log("Hola mundo");

app.listen(port); //Nos ponemos a escuchar en el puerto.
console.log("API escuchando en el puerto " + port);

app.get('/apitechu/v5/usuarios', function(req, res) //http://localhost:3000/apitechu/v5/usuarios
{
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey);
  clienteMlab.get('', function(err, resM, body)
  {
    if(!err) res.send(body);
  });
});

app.get('/apitechu/v5/usuarios/:id', function(req, res) //http://localhost:3000/apitechu/v5/usuarios
{
  var id = req.params.id;
  //var query = 'q={"id":' + id + '}';
  var query = 'q={"id":' + id + '}&f={"first_name":1, "last_name":1, "_id":0}';
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey);
  clienteMlab.get('', function(err, resM, body)
  {
    if(!err)
      if(body.length >0) res.send(body[0]);
      else res.status(404).send('Usuario no encontrado');
  });
});

app.post('/apitechu/v5/login', function(req, res) //http://localhost:3000/apitechu/v5/login
{
  var email = req.headers.email;
  var password = req.headers.password;
  var currentUser;

  var query = 'q={"email":"' + email + '", "password":"' + password +
  '"}&f={"id":1, "nombre":1, "apellido":1, "_id":0}';
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey);
  clienteMlab.get('', function(err, resM, body)
  {
    if(!err)
    {
      if(body.length == 1)
      {
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios");
        var cambio = '{"$set":{"logged":true}}';
        clienteMlab.put('?q={"id":'+ body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP)
        {
          if(!errP)
          {
          res.send({"login":"ok", "id":body[0].id, "nombre": body[0].nombre,
            "apellido": body[0].apellido});
          }
          else res.send("Error en login");
        });
      }
      else res.status(404).send('Usuario no encontrado');
    }
  });
});

app.post('/apitechu/v5/logout', function(req, res) //http://localhost:3000/apitechu/v5/logout
{
  var id = req.headers.id;
  var currentUser;

  var query = 'q={"id":' + id + ', "logged":true}&f={"id":1, "_id":0}';
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey);
  clienteMlab.get('', function(err, resM, body)
  {
    if(!err)
    {
      if(body.length == 1)
      {
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios");
        var cambio = '{"$set":{"logged":false}}';
        clienteMlab.put('?q={"id":'+ body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP)
        {
          if(!errP)
          {
          res.send({"logout":"ok", "id":body[0].id});
          }
          else res.send("Error en logout");
        });
      }
      else
      {
        if(body.length == 0) res.status(404).send('Usuario no logado previamente');
        else res.send("Error en obtención de usuario");
      }
    }
  });
});

app.get('/apitechu/v1', function(req, res)//http://localhost:3000/apitechu/v1
{
  //console.log((req));
  res.send({"mensaje":"Bienvenido a mi API"});
});

app.get('/apitechu/v1/usuarios', function(req, res) //http://localhost:3000/apitechu/v1/usuarios
{
    res.send(usuarios);
});

app.post('/apitechu/v1/usuarios', function(req, res) //http://localhost:3000/apitechu/v1/usuarios
{
  var nuevo = { "first_name":req.headers.first_name,
                "country": req.headers.country};
  usuarios.push(nuevo);
  console.log(req.headers);
  const datos = JSON.stringify(usuarios);
  fs.writeFile("./usuarios.json", datos, "utf8", function(err)
  {
    if(err)
    {
      console.log(err);
    }else
    {
      console.log("Fichero guardado");
    }
  });
  res.send("Alta OK");
});

app.delete('/apitechu/v1/usuarios/:id', function(req, res) //http://localhost:3000/apitechu/v1/usuarios/2
{
  usuarios.splice(req.params.id-1, 1);
  res.send("Usuario borrado");
});

app.post('/apitechu/v1/monstruo/:p1/:p2', function(req, res) //http://localhost:3000/apitechu/v1/monstruo/Beatriz/MR?extra=coffee
{
  console.log("Parámetros");
  console.log(req.params);
  console.log("Query Strings");
  console.log(req.query);
  console.log("Headers");
  console.log(req.headers);
  console.log("Body");
  console.log(req.body);
});

app.post('/apitechu/v2/usuarios', function(req, res) //http://localhost:3000/apitechu/v2/usuarios
{
  //var nuevo = { "first_name":req.headers.first_name,
  //              "country": req.headers.country};
  var nuevo = req.body;
  usuarios.push(nuevo);
  //console.log(req.headers);
  const datos = JSON.stringify(usuarios);
  fs.writeFile("./usuarios.json", datos, "utf8", function(err)
  {
    if(err)
    {
      console.log(err);
    }else
    {
      console.log("Fichero guardado");
    }
  });
  res.send("Alta OK");
});

app.post('/apitechu/v2/usuarios', function(req, res) //http://localhost:3000/apitechu/v2/usuarios
{
  //var nuevo = { "first_name":req.headers.first_name,
  //              "country": req.headers.country};
  var nuevo = req.body;
  usuarios.push(nuevo);
  //console.log(req.headers);
  const datos = JSON.stringify(usuarios);
  fs.writeFile("./usuarios.json", datos, "utf8", function(err)
  {
    if(err)
    {
      console.log(err);
    }else
    {
      console.log("Fichero guardado");
    }
  });
  res.send("Alta OK");
});

app.post('/apitechu/v2/login', function(req, res) //http://localhost:3000/apitechu/v2/login
{
  var email = req.headers.email;
  var password = req.headers.password;
  var encontrado = 0;
  var currentUser;

  for (var i = 0; i < logins.length; i++)
  {
    if (logins[i].email == email && logins[i].password == password)
    {
      logins[i].logged= true;
      encontrado = logins[i].id;
      currentUser = logins;
      break;
    }
  }
  if(encontrado!=0)
  {
    const datos = JSON.stringify(currentUser);
    fs.writeFile("./logins.json", datos, "utf8", function(err)
    {
      if(err)
      {
        console.log(err);
      }else
      {
        console.log("Fichero guardado");
      }
    });

    res.send("Login OK --> Id: " + encontrado);
  }
  else res.send("Login KO");
});

app.post('/apitechu/v2/logout', function(req, res) //http://localhost:3000/apitechu/v2/logout
{
  var id = req.headers.id;
  var encontrado = 0;
  var currentUser;

  for (var i = 0; i < logins.length; i++)
  {
    if (logins[i].id == id && logins[i].logged == true)
    {
      logins[i].logged = false;
      encontrado = logins[i].id;
      currentUser = logins;
      break;
    }
  }
  if(encontrado!=0)
  {
    const datos = JSON.stringify(currentUser);
    fs.writeFile("./logins.json", datos, "utf8", function(err)
    {
      if(err)
      {
        console.log(err);
      }else
      {
        console.log("Fichero guardado");
      }
    });

    res.send("Logout OK --> Id: " + encontrado);
  }
  else res.send("Login KO");
});

//------------------------BLOQUE CUENTAS------------------------
app.get('/apitechu/v2/cuentas', function(req, res) //http://localhost:3000/apitechu/v2/cuentas
{
  var currentCuentas = [];

  for (var i = 0; i < cuentas.length; i++)
  {
    currentCuentas.push(cuentas[i].iban);
  }
    res.send(currentCuentas);
});

app.get('/apitechu/v2/cuentas/usuario', function(req, res) //http://localhost:3000/apitechu/v2/cuentas/usuario
{
  var idcliente = req.headers.idcliente;
  var encontrado = false;
  var currentCuentas = [];

  for (var i = 0; i < cuentas.length; i++)
  {
    if (cuentas[i].idcliente == idcliente)
    {
      currentCuentas.push(cuentas[i].iban);
      encontrado = true;
    }
  }
  if(encontrado)
  {
    res.send(currentCuentas);
  }
  else res.send("Cuentas no encontradas");
});

app.get('/apitechu/v5/cuentas/usuario', function(req, res) //http://localhost:3000/apitechu/v5/cuentas/usuario
{
  var idcliente = req.headers.idcliente;
  var currentUser;

  var query = 'q={"idcliente":' + idcliente + '}&f={"iban":1, "_id":0}';
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey);
  clienteMlab.get('', function(err, resM, body)
  {
    if(!err)
    {
      if(body.length > 0) res.send(body);
      else res.status(404).send('Cuentas no encontradas');
    }
  });
});

app.get('/apitechu/v5/movimientos', function(req, res) //http://localhost:3000/apitechu/v5/movimientos
{
  var iban = req.headers.iban;
  var currentUser;

  var query = 'q={"iban":"' + iban + '"}&f={"movimientos":1, "_id":0}';
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey);
  clienteMlab.get('', function(err, resM, body)
  {
    if(!err)
    {
      if(body.length > 0) res.send(body);
      else res.status(404).send('Movimientos no encontrados');
    }
  });
});

app.get('/apitechu/v2/movimientos', function(req, res) //http://localhost:3000/apitechu/v2/movimientos
{
  var iban = req.headers.iban;
  var encontrado = false;
  var currentMovimientos = [];

  for (var i = 0; i < cuentas.length; i++)
  {
    if (cuentas[i].iban == iban)
    {
      currentMovimientos.push(cuentas[i].movimientos);
      encontrado = true;
    }
  }
  if(encontrado)
  {
    res.send(currentMovimientos);
  }
  else res.send("Movimientos no encontrados");
});
